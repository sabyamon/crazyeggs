<!-- The top of file index.html -->
<html>
<head>
	
	<script src="plusone.js" type="text/javascript"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>


<!-- Enabling Google+ login for Thek -->
  <script type="text/javascript">
  /*
   * Triggered when the user accepts the sign in, cancels, or closes the
   * authorization dialog.
   */
  function loginFinishedCallback(authResult) {
    if (authResult) {
      if (authResult['error'] == undefined){
        gapi.auth.setToken(authResult); // Store the returned token.
        var emailId;

        gapi.client.load('oauth2', 'v2', function() { // Loads 
                var request = gapi.client.oauth2.userinfo.get();
                request.execute(function(resp) {
                console.log(resp);
                emailId = resp.email;
              });
            });

    gapi.client.load('plus','v1', function() {
          var request = gapi.client.plus.people.get({
           'userId': 'me'
            });
          request.execute(function(resp) {
          callProcessor(resp,emailId);
            });
        });
    
      } else {
        console.log('An error occurred');
      }
    } else {
      console.log('Empty authResult');  // Something went wrong
    }
  }



  <!-- Function which calls the processor script with user data -->
  function callProcessor(resp,emailId){

      console.log('Retrieved Email for:' + emailId);
      console.log('Retrieved Display Name for:' + resp.displayName);
      console.log('Retrieved Gender for:' + resp.gender);
      console.log('Retrieved objectType for:' + resp.objectType);
      console.log('Retrieved image.url for:' + resp.image.url);
      console.log('Retrieved language for:' + resp.language);
      console.log('Retrieved ageRange.min for:' + resp.ageRange.min);

      var displayname = resp.displayName ;

      console.log('Creating the rest url for registering a user');

      var url = 'localhost:8985/?' + 'action=' + 'insert' + '&' + 'name=' + resp.displayName + '&' + 'email=' + emailId +
                '&' + 'gender=' + resp.gender + '&' + 'authCode=' + '12345' ;

      // localhost:8985/action=insert&name=Sabyasachi Mukherjee&email=sabyamon@gmail.comgender=male&authCode=12345
      console.log('url to be called is ');
      console.log(url);


      var jsonData = {
                      "action" : "registerUser",
                      "name" : resp.displayName , 
                      "email" : emailId , 
                      
                      };

      console.log('json data is' + JSON.stringify( jsonData ));

  //   $.ajax({
  //      url: 'localhost:8985', //This is the current doc
  //      type: "POST",
  //      datatype : jsonp;
  //      data: JSON.stringify( jsonData ),
  //      success: function(data){
  //       alert('inside success') ;
  //        console.log('data coming from google/fb is ' +data);
  //     },
  //     error: function(XMLHttpRequest, textStatus, errorThrown){
  //       console.log('Its an error!!') ;
  //       console.log('issue is ' + errorThrown);
  //       console.log('test status is ' + textStatus);

  //     }

  // });

  }
  </script>



<!-- Facebook Login Code -->

<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '628587837152963', // App ID
    channelUrl : 'channel.html', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
    callProcessorFB(response);

    });
  }


  <!-- Function which calls the processor script with user data -->
  function callProcessorFB(resp){

      console.log('Facebook response is :: '+resp);
      console.log('Retrieved facebook Email for:' + resp.email);
      console.log('Retrieved facebook Display Name for:' + resp.name);
      console.log('Retrieved facebook Gender for:' + resp.gender);
      console.log('Retrieved facebook birthday for:' + resp.birthday);

      //email,user_about_me,user_birthday,user_hometown,user_interests,user_likes,user_location,user_photos
      console.log('Retrieved facebook link for:' + resp.link);
      console.log('Retrieved facebook likes for:' + resp.likes);
      console.log('Retrieved facebook user location for:' + resp.location);
      console.log('Retrieved facebook user photos for:' + resp.photos);

      //console.log('Retrieved facebook image.url for:' + resp.image.url);
      //console.log('Retrieved facebook language for:' + resp.language);
      //console.log('Retrieved facebook ageRange.min for:' + resp.ageRange.min);

      $.ajax({
       url: 'processor.php', //This is the current doc
       type: "POST",
       dataType:'json', // add json datatype to get json
       data: ({displayName : resp.name,EmailID : resp.email , login_provider : 'facebook'}),
       success: function(data){
         console.log(data);
      }
      });  


  }

</script>



<!-- facebook Login Code Ends Here -->


	
</head>



<div>
            
              <div id="signin-button" class="show" style="float:left">
               <div class="g-signin" data-callback="loginFinishedCallback"
                  data-approvalprompt="force"
                  data-clientid="526697610145.apps.googleusercontent.com"
                  data-scope="https://www.googleapis.com/auth/userinfo.email"
                  data-height="short"
                  data-cookiepolicy="single_host_origin"
                  >
                </div>
              <!-- In most cases, you don't want to use approvalprompt=force. Specified
              here to facilitate the demo.-->
              </div>

              <br/>
              <br/>
              

              
              <!-- Add where you want your sign-in button to render -->

              <!-- Facebook Button -->
              <fb:login-button show-faces="true" width="400" max-rows="1" scope="email,user_about_me,user_birthday,user_hometown,user_interests,user_likes,user_location,user_photos"></fb:login-button>
              <!-- Facebook Button Ends -->

            </div>  
            

          </div>
  
  


</html>