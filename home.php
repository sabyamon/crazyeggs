
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Crazy Eggs!</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="thekker.css" rel="stylesheet">
    <script src="jquery-1.10.2.js"></script>
    <script src="plusone.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom styles for this template -->
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->


    <!-- Enabling Google+ login for Thek -->
  <script type="text/javascript">

  $(document).ready(function(){

 function authenticateUser(data){
    console.log('inside authenicated data .. hence data inserted into db');
  }

});

  /*
   * Triggered when the user accepts the sign in, cancels, or closes the
   * authorization dialog.
   */
  function loginFinishedCallback(authResult) {
    if (authResult) {
      if (authResult['error'] == undefined){
        gapi.auth.setToken(authResult); // Store the returned token.
        var emailId;

        gapi.client.load('oauth2', 'v2', function() { // Loads 
                var request = gapi.client.oauth2.userinfo.get();
                request.execute(function(resp) {
                console.log(resp);
                emailId = resp.email;
              });
            });

    gapi.client.load('plus','v1', function() {
          var request = gapi.client.plus.people.get({
           'userId': 'me'
            });
          request.execute(function(resp) {
          callProcessor(resp,emailId);
            });
        });
    
      } else {
        console.log('An error occurred');
      }
    } else {
      console.log('Empty authResult');  // Something went wrong
    }
  }



  <!-- Function which calls the processor script with user data -->
  function callProcessor(resp,emailId){

      console.log('Retrieved Email for:' + emailId);
      console.log('Retrieved Display Name for:' + resp.displayName);
      console.log('Retrieved Gender for:' + resp.gender);
      console.log('Retrieved objectType for:' + resp.objectType);
      console.log('Retrieved image.url for:' + resp.image.url);
      console.log('Retrieved language for:' + resp.language);
      console.log('Retrieved ageRange.min for:' + resp.ageRange.min);

      var displayname = resp.displayName ;

      console.log('Creating the rest url for registering a user');

      var apiURL = 'localhost:8985/?' + 'action=' + 'insert' + '&' + 'name=' + 'Sabyasachi' + '&' + 'email=' + emailId +
                '&' + 'gender=' + resp.gender + '&' + 'authCode=' + '12345&callback=authenticateUser';

      // localhost:8985/action=insert&name=Sabyasachi Mukherjee&email=sabyamon@gmail.comgender=male&authCode=12345
      console.log('url to be called is ');
      console.log(apiURL);

      //console.log('json data is' + JSON.stringify( jsonData ));

    $.ajax({
           url: apiURL, //This is the current doc
           //type: "POST",
           dataType : 'jsonp',
           jsonpCallback: 'authenticateUser',
            jsonp: 'callback',
           //jsonpCallback: 'authenticateUser',
           //jsonp: false,
           //data: JSON.stringify( jsonData ),
           success: function(data){
            //authenticateUser(data);
            alert('inside success') ;
             console.log('data coming from google/fb is ' +data);
          },
          error: function(XMLHttpRequest, textStatus, errorThrown){
            console.log('Its an error!!') ;
            console.log('issue is ' + errorThrown);
            console.log('test status is ' + textStatus);

          },
        });

  }

 

  </script>



<!-- Facebook Login Code -->

<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '628587837152963', // App ID
    channelUrl : 'channel.html', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct interaction from people using the app (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
    callProcessorFB(response);

    });
  }


  <!-- Function which calls the processor script with user data -->
  function callProcessorFB(resp){

      console.log('Facebook response is :: '+resp);
      console.log('Retrieved facebook Email for:' + resp.email);
      console.log('Retrieved facebook Display Name for:' + resp.name);
      console.log('Retrieved facebook Gender for:' + resp.gender);
      console.log('Retrieved facebook birthday for:' + resp.birthday);

      //email,user_about_me,user_birthday,user_hometown,user_interests,user_likes,user_location,user_photos
      console.log('Retrieved facebook link for:' + resp.link);
      console.log('Retrieved facebook likes for:' + resp.likes);
      console.log('Retrieved facebook user location for:' + resp.location);
      console.log('Retrieved facebook user photos for:' + resp.photos);

      //console.log('Retrieved facebook image.url for:' + resp.image.url);
      //console.log('Retrieved facebook language for:' + resp.language);
      //console.log('Retrieved facebook ageRange.min for:' + resp.ageRange.min);

      $.ajax({
       url: 'processor.php', //This is the current doc
       type: "POST",
       dataType:'json', // add json datatype to get json
       data: ({displayName : resp.name,EmailID : resp.email , login_provider : 'facebook'}),
       success: function(data){
         console.log(data);
      }
      });  


  }

</script>

<script type="text/javascript">

$(document).ready(function($) {
  
//  $('#snapPitch').hide();
  var counter = 2 ;
  rotator(counter);
  
  function rotator(counter){
    //console.log('in rotator');
     window.setTimeout(function(){

      if((counter % 2) === 0){
        //console.log('even');
         //$('#snapPitch').html('<p>even one</p>');
         $('#snapPitch').html('<p><b>Music is in your blood ! But unless you are in Indian Idol , how come Anu Malik will recognize you ?</b></p><p>Upload your composition to Crazy Eggs ! We will make it viral and bring it to notice of the Janta !</p>');
         $('#snapPitch').fadeIn('slow');
      }else{
        //console.log('odd');
        $('#snapPitch').html('<p><b>You are an in-born photographer and take jaw dropping snaps !</b></p><p>What is use of your talent if that is not known to people ?</p>');
        $('#snapPitch').fadeIn('slow');
      }
      counter ++ ;
      rotator(counter);

     },3500);
   }

});

</script>


  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Crazy Eggs!</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#snapbuff">SnapBuffs</a></li>
            <li><a href="#musicians">Musicians</a></li>
            
            <li><a href="#chatroom">Chatroom</a></li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li> -->
          </ul>
          <form class="navbar-form navbar-right">

            <div>
            
              <div id="signin-button" class="show" style="float:left">
               <div class="g-signin" data-callback="loginFinishedCallback"
                  data-approvalprompt="force"
                  data-clientid="526697610145.apps.googleusercontent.com"
                  data-scope="https://www.googleapis.com/auth/userinfo.email"
                  data-height="short"
                  data-cookiepolicy="single_host_origin"
                  >
                </div>
              <!-- In most cases, you don't want to use approvalprompt=force. Specified
              here to facilitate the demo.-->
              </div>
              <!-- Facebook Button -->
              <div style="float:right !important;margin-left:20px"><fb:login-button show-faces="true" width="200" max-rows="1" scope="email,user_about_me,user_birthday,user_hometown,user_interests,user_likes,user_location,user_photos"></fb:login-button></div>
              <!-- Facebook Button Ends -->

            </div>  
            <!-- <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button> -->
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Welcome to Crazy Eggs !! </h1>
        <p><i>Crazy Eggs is a platform for collaboration of people with creative mind. Even if you think that you are not creative enough , do come. Its our responsibility to get that creative piece out of you.</i></p>
        <div id="snapPitch">
          <p><b>You are an in-born photographer and take jaw dropping snaps !</b></p>
          <p>What is use of your talent if that is not known to people ?</p>
        </div>
        <p><a class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
        </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
       </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#">View details &raquo;</a></p>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; Company 2013</p>
      </footer>
    </div> <!-- /container -->
    
  </body>
</html>
